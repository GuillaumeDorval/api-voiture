FROM openjdk:12-alpine as build

COPY cars-api.jar /usr/src/myapp

WORKDIR /usr/src/myapp

EXPOSE 5000

CMD ["java", "cars-api.jar"]